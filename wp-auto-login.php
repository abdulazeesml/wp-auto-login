<?php

namespace SMAARTWEB\WPAutoLogin;

use SMAARTWEB\WPAutoLogin\Network\NetworkMain;

/** بسم الله الرحمن الرحيم **
 *
 * Plugin Name: WP Auto Login
 * Plugin URI: http://www.smaartweb.com
 * Version: 1.0
 * Description: Used to Auto Login Another Site
 * Author: Azees
 * Author URI: http://smaartweb.com
 * Domain Path: /languages/
 * Text Domain: aqpb-l10n
 *
 * -------------------------------------------------------------------------------
 * Copyright (c) 2014 SmaartWeb. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */
if ( !defined( 'WPINC' ) ) {
    die;
}

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
$AutoLoginNetwork = new NetworkMain();
