<?php

namespace SMAARTWEB\WPAutoLogin\Network\Admin;

class NetworkAdmin {

    /**
     * Default constructor
     * @access public
     */
    public function __construct() {
        add_action( 'network_admin_menu', [$this, 'add_menu'] );
        add_action( 'admin_init', [$this, 'register_settings'] );
        add_action( 'admin_init', [$this, 'save_meta'], 10, 2 );
    }

    /**
     * Adds the profile menu item
     * @access public
     */
    public function add_menu() {
        $this->page = add_menu_page( 'Auto Login', 'Auto Login', 'network_options', 'auto-login', [$this, 'network_page'], 'dashicons-admin-generic' );
        add_action( "load-" . $this->page, [$this, 'enque_settings_js'] );
        add_action( "load-" . $this->page, [$this, 'add_settings_metabox'] );
    }

    /**
     * Adds all the settings scripts
     * @access public
     */
    public function enque_settings_js() {
        $bower_url = plugin_dir_url( dirname( __FILE__ ) );
        wp_enqueue_script( 'common' );
        wp_enqueue_script( 'wp-lists' );
        wp_enqueue_script( 'postbox' );
        wp_enqueue_script( 'wp-auto-login-network-settings', plugins_url( 'js/settings.js', __FILE__ ), ['jquery'] );
        wp_enqueue_script( 'blockUI', $bower_url . 'blockUI/jquery.blockUI.js', ['fbrilla-settings'] );
        $data = ['nonce' => wp_create_nonce( 'auto-login-action' )];
        wp_localize_script( 'auto-login-settings', 'auto_login_vars', $data );
    }

    /**
     * Render the settings page
     * @access public
     */
    public function network_page() {
        require_once 'views/main.php';
    }

    /**
     * Load settings metaboxes
     * @access public
     */
    public function add_settings_metabox() {
        add_meta_box( 'auto-login-sites', 'Autologin Sites', [$this, 'process_metabox'], 'toplevel_page_pcf-user-dash', 'normal', 'core', ['view' => 'views/sites.php'] );
    }

    /**
     * process and display the metabox
     * @access public
     */
    public function process_metabox( $box, $args ) {
        require_once $args['args']['view'];
    }

    
    /**
     * Register network settings
     * @access public
     * @param datatype $var description
     */
    public function register_settings() {
        if ( is_main_site() ) {
            register_setting( 'wp-autologin-network-settings-group', 'wpautologin_settings' );
        }
    }

}
