jQuery(document).ready(function () {
    'use strict';

    var dash = jQuery(".dash-roles-wrapper:last").clone();

    jQuery(document).on("click", ".add-dash", function (e) {
        e.preventDefault();
        var $this = jQuery(this);
        var closest_dash = $this.closest(".dash-roles-wrapper");
        var counter = jQuery(".dash-roles-wrapper").length;
        var dash_clone = dash.clone();
        var multi_select = dash_clone.find('.user_dash_roles');
        multi_select.attr('name', 'wpautologin_settings[roles][' + counter + '][]');
        multi_select.multiselect({
            selectedText: "# of # roles selected",
            noneSelectedText: 'Select Roles'
        });
        multi_select.multiselect("uncheckAll");
        multi_select.multiselect("refresh");
        dash_clone.find("[name^=wpautologin_settings\\[title\\]]").attr("name", 'wpautologin_settings[title][' + counter + ']').val('');
        dash_clone.find("[name^=wpautologin_settings\\[url\\]]").attr("name", 'wpautologin_settings[url][' + counter + ']').val('');
        closest_dash.after(dash_clone);
    });

    jQuery(document).on("click", ".remove-dash", function (e) {
        e.preventDefault();
        var $this = jQuery(this);
        $this.closest(".dash-roles-wrapper").remove();
        rearrange_dashes();
    });

    jQuery(".user_dash_roles").multiselect({
        selectedText: "# of # roles selected",
        noneSelectedText: 'Select Roles'
    });

    jQuery('.wp-media-popper').wp_media_popper();

    function rearrange_dashes() {
        var count = 0;
        jQuery(".dash-roles-wrapper").each(function () {
            var $this = jQuery(this);
            $this.find("[name^=user_dash_options\\[title\\]]").attr("name", 'user_dash_options[title][' + count + ']');
            $this.find("[name^=user_dash_options\\[url\\]]").attr("name", 'user_dash_options[url][' + count + ']');
            $this.find('.user_dash_roles').attr("name", 'user_dash_options[roles][' + count + '][]');
            count += 1;
        });
    }

});
