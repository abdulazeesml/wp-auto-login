<?php
global $wp_roles;
$opts = get_option( 'wpautologin_settings' );
if ( count( $opts['title'] ) > 1 ) {
    $titles = $opts['title'];
    $urls = $opts['url'];
    $roles = $opts['roles'];
    $title_object = new ArrayObject( $titles );
    $itert = $title_object->getIterator();
    while ( $itert->valid() ) {
        ?>
        <table class="form-table dash-roles-wrapper">
            <tbody>
                <tr valign="top">
                    <th scope="row">Menu Title</th>
                    <td>
                        <input type="text" name="wpautologin_settings[title][<?php echo $itert->key(); ?>]" value="<?php echo $titles[$itert->key()]; ?>" class="regular-text">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Menu URL</th>
                    <td>
                        <input type="text" name="wpautologin_settings[url][<?php echo $itert->key(); ?>]" value="<?php echo $urls[$itert->key()]; ?>" class="large-text">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Menu Roles</th>
                    <td>
                        <select name="wpautologin_settings[roles][<?php echo $itert->key(); ?>][]" multiple="multiple" class="user_dash_roles">
                            <?php
                            $roles_arr = $roles[$itert->key()];
                            foreach ( $wp_roles->role_names as $role => $label ) {
                                $selected = in_array( $role, $roles_arr );
                                echo '<option value="' . $role . '"' . selected( TRUE, $selected, FALSE ) . '>' . $label . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>
                        <a href="#" class="gh-button icon add add-dash">Add Dash</a>
                        <a href="#" class="gh-button icon trash danger remove-dash">Delete</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
        $itert->next();
    }
} else {
    ?>
    <table class="form-table dash-roles-wrapper">
        <tbody>
            <tr valign="top">
                <th scope="row">Menu Title</th>
                <td>
                    <input type="text" name="wpautologin_settings[title][0]" value="<?php echo $opts['title'][0]; ?>" class="regular-text">
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Menu URL</th>
                <td>
                    <input type="text" name="wpautologin_settings[url][0]" value="<?php echo $opts['url'][0]; ?>" class="large-text">
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Menu Roles</th>
                <td>
                    <select name="wpautologin_settings[roles][0][]" multiple="multiple" class="user_dash_roles">
                        <?php
                        $roles_arr = $opts['roles'][0];
                        foreach ( $wp_roles->role_names as $role => $label ) {
                            $selected = in_array( $role, $roles_arr );
                            echo '<option value="' . $role . '"' . selected( TRUE, $selected, FALSE ) . '>' . $label . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th scope="row"></th>
                <td>
                    <a href="#" class="gh-button icon add add-dash">Add Dash</a>
                    <a href="#" class="gh-button icon trash danger remove-dash">Delete</a>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}