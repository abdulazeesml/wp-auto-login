<?php
global $screen_layout_columns, $wp_roles;
$plugin_options = get_option( 'auto-login-sites' );
?>
<div class="wrap">
    <?php screen_icon( 'options-general' ); ?>
    <h2>PCF User Dashboard</h2>
    <?php settings_errors(); ?>
    <form method="post" action="<?php echo admin_url( 'options.php' ); ?>">
        <?php settings_fields( 'wp-autologin-network-settings-group' ); ?>
        <?php wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', FALSE ); ?>
        <?php wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', FALSE ); ?>
        <div id="poststuff" class="metabox-holder<?php echo 2 == $screen_layout_columns ? ' has-right-sidebar' : ''; ?>">
            <div id="side-info-column" class="inner-sidebar">
                <?php do_meta_boxes( 'toplevel_page_pcf-user-dash', 'side', '' ); ?>
            </div>
            <div id="post-body" class="has-sidebar">
                <div id="post-body-content" class="has-sidebar-content">
                    <?php do_meta_boxes( 'toplevel_page_pcf-user-dash', 'normal', '' ); ?>
                </div>
            </div>
        </div>
        <?php submit_button(); ?>
    </form>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.if-js-closed').removeClass('if-js-closed').addClass('closed');
        postboxes.add_postbox_toggles('toplevel_page_pcf-user-dash');
    });
</script>