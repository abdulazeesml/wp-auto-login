<?php
global $post;
$rtm_codes = get_post_meta( $post->ID, '_rtmojo_codes', TRUE );
$rtm_titles = get_post_meta( $post->ID, '_rtmojo_titles', TRUE );
$rtm_locations = get_post_meta( $post->ID, '_rtmojo_locations', TRUE );

/**
 * Old plugin fix
 */
if ( !$rtm_codes ) {
    $old_fb_pixel_code = html_entity_decode( get_post_meta( $post->ID, '_rtmojo_header', TRUE ), ENT_QUOTES );
    if ( $old_fb_pixel_code ) {
        $rtm_codes[0] = $old_fb_pixel_code;
        $rtm_titles[0] = 'Facebook Pixel';
        $rtm_locations[0] = 'header';
    }

    $old_tracking_code = html_entity_decode( get_post_meta( $post->ID, '_rtmojo_footer', TRUE ), ENT_QUOTES );
    if ( $old_tracking_code ) {
        $rtm_codes[1] = $old_tracking_code;
        $rtm_titles[1] = 'Tracking Code';
        $rtm_locations[1] = 'footer';
    }
}

$rtm_codes_obj = new ArrayObject( (array) $rtm_codes );
$rtm_iter = $rtm_codes_obj->getIterator();
if ( $rtm_iter->count() > 0 ) {
    while ( $rtm_iter->valid() ) {
        $rtm_title = $rtm_titles[$rtm_iter->key()];
        $rtm_loc = $rtm_locations[$rtm_iter->key()];
        $rtm_code = html_entity_decode( $rtm_codes[$rtm_iter->key()], ENT_QUOTES );
        ?>
        <table class="form-table global-code-wrapper">
            <tbody>
                <tr valign="top">
                    <th scope="row">Pixel Title</th>
                    <td>
                        <input type="text" name="rtmojo_title[<?php echo $rtm_iter->key(); ?>]" class="large-text" value="<?php echo $rtm_title; ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Pixel Location</th>
                    <td>
                        <select name="rtmojo_location[<?php echo $rtm_iter->key(); ?>]">
                            <option value="header"<?php selected( 'header', $rtm_loc ); ?>>Header</option>
                            <option value="footer"<?php selected( 'footer', $rtm_loc ); ?>>Footer</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Pixel Code</th>
                    <td>
                        <textarea class="large-text" rows="5" name="rtmojo_code[<?php echo $rtm_iter->key(); ?>]"><?php echo $rtm_code; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>
                        <a href="#" class="gh-button icon add add-global-code">Add Pixel</a>
                        <a href="#" class="gh-button icon trash danger remove-global-code">Delete</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
        $rtm_iter->next();
    }
} else {
    ?>
    <table class="form-table global-code-wrapper">
        <tbody>
            <tr valign="top">
                <th scope="row">Pixel Title</th>
                <td>
                    <input type="text" name="rtmojo_title[0]" class="large-text" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Pixel Location</th>
                <td>
                    <select name="rtmojo_location[0]">
                        <option value="header">Header</option>
                        <option value="footer">Footer</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th scope="row">Pixel Code</th>
                <td>
                    <textarea class="large-text" rows="5" name="rtmojo_code[0]"></textarea>
                </td>
            </tr>
            <tr>
                <th scope="row"></th>
                <td>
                    <a href="#" class="gh-button icon add add-global-code">Add Pixel</a>
                    <a href="#" class="gh-button icon trash danger remove-global-code">Delete</a>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}
?>